
package notation

import (
	"math"
	"unicode/utf8"
)

func stringInSlice(a string, list []string) int64 {
    for i, b := range list {
        if b == a {
	    return int64(i)
        }
    }
    return -1
}


func IntToN(from int64,toBase *[]string) (string){
    var sign string
    base:=int64(len(*toBase))
    if from<0 {
      sign="-"
      from= -from
    }else{
      sign=""
    }
    m:=from%base
    d:=from/base
    if d>=base {return sign+IntToN(d,toBase)+(*toBase)[m]} else {return sign+(*toBase)[d]+(*toBase)[m]}
}

func MtoN(fromString string, fromBase *[]string, toBase *[]string) (string){
  return IntToN(NtoInt(fromString,fromBase),toBase)
}

func nToIntPos(fromString string, fromBase *[]string, pos int64)(int64){
  r, size := utf8.DecodeLastRuneInString(fromString)
  cur := stringInSlice(string(r),*fromBase)*int64(math.Pow(float64(len(*fromBase)),float64(pos)))
  if utf8.RuneCountInString(fromString[:len(fromString)-size])>0 {
    return cur+nToIntPos(fromString[:len(fromString)-size], fromBase, pos+1)
  } else {return cur}
}

func NtoInt(fromString string, fromBase *[]string)(int64){
  pos :=int64(0)
  r, size := utf8.DecodeLastRuneInString(fromString)
  cur := stringInSlice(string(r),*fromBase)*int64(math.Pow(float64(len(*fromBase)),float64(pos)))
  if fromString[0] == '-' {
    if utf8.RuneCountInString(fromString[1:len(fromString)-size])>0 {
      return -1*(cur+nToIntPos(fromString[1:len(fromString)-size], fromBase, pos+1))
    } else {return -cur}
  }else{
    if utf8.RuneCountInString(fromString[:len(fromString)-size])>0 {
      return cur+nToIntPos(fromString[:len(fromString)-size], fromBase, pos+1)
    } else {return cur}}
}